import itertools
import string
from collections import Counter
import math

# englishLetterFreq = {'E': 12.70, 'T': 9.06, 'A': 8.17, 'O': 7.51, 'I': 6.97, 'N': 6.75, 'S': 6.33, 'H': 6.09, 'R': 5.99, 'D': 4.25, 'L': 4.03, 'C': 2.78, 'U': 2.76, 'M': 2.41, 'W': 2.36, 'F': 2.23, 'G': 2.02, 'Y': 1.97, 'P': 1.93, 'B': 1.29, 'V': 0.98, 'K': 0.77, 'J': 0.15, 'X': 0.15, 'Q': 0.10, 'Z': 0.07}


def letters():
    alphabet_string = string.ascii_uppercase
    return list(alphabet_string)


def shift(text, shift_by):
    alphabet = letters()
    shifted = ""
    for x in text:
        index = alphabet.index(x)
        shift_index = index + shift_by
        if shift_index < 0:
            shift_index += len(alphabet)
        elif shift_index >= len(alphabet):
            shift_index -= len(alphabet)
        shifted += alphabet[shift_index]
    return shifted


def frequency(list_of_strings):
    """ Counts the occurrence of a string in list
    Ordered by occurrence desc
    """

    return Counter(list_of_strings)


def replace(text, substitutions):
    for original, substitution in substitutions:
        text = text.replace(original, substitution)
    print(text)


def nlets(text, nlet_len):
    """ Separates text into substrings of nlet_len length

    Ex: text='example', nlet_len=5, returns: [examp, xampl, ample]
    """

    nlets = []
    for i in range(0, len(text) - nlet_len + 1):
        nlets.append(text[i:i + nlet_len])
    return nlets


def ic(text):
    """ index of coincidence
    ~0.038 for random
    ~0.065 for meaningful
    """

    ioc = 0
    f = frequency(text)
    for letter, count in f.items():
        first = count / len(text)
        second = (count - 1) / (len(text) - 1)
        ioc += first * second
    return ioc


def ics(groups):
    """ Calculates ics of all (string) values in list """
    ics = []
    while groups:
        ics.append(ic(groups.pop(0)))
    return ics


def mutual_ic(x, y):
    """ mutual index of coincidence """

    f_x = frequency(x)
    f_y = frequency(y)
    mic = 0
    for letter in letters():
        f_xv = f_x.get(letter, 0)
        f_yv = f_y.get(letter, 0)
        mic += (f_xv / len(x)) * (f_yv / len(y))
    return mic


def mutual_ic_shift(x, y, total_shift_count=26):
    """ Calculates mutual ic for every g (every shift of y) """
    mics = []
    for g in range(total_shift_count):
        y_shifted = shift(y, g)
        mics.append(mutual_ic(x, y_shifted))
    return mics


def mutual_ic_shift_all(groups, total_shift_count=26):
    """ Calculates mutual ic for every group pair (combination) for every g (every shift of y) """
    combinations = itertools.combinations(groups, 2)
    mics = []
    for combination in combinations:
        index_1 = groups.index(combination[0])
        index_2 = groups.index(combination[1])
        mics.append({"x": f"z_{index_1+1}", "y": f"z_{index_2+1}", "mic": mutual_ic_shift(combination[0], combination[1], total_shift_count)})
    return mics


def gcd(*args):
    """ Greatest common divisor """
    gcd_res = args[0]
    for i in range(len(args) - 1):
        gcd_res = math.gcd(gcd_res, args[i + 1])
    return gcd_res


def indexes(text, substring):
    """ Finds all indexes of substring in string """
    return [i for i in range(len(text)) if text.startswith(substring, i)]


def index_diff(indexes):
    """ Finds the differences of the indexes based on first index
    Ex: [1, 3, 5] returns [2, 4]
    """
    first = indexes.pop(0)
    diffs = []
    while indexes:
        diffs.append(indexes.pop(0) - first)
    return diffs


def shift_groups(text, shift_by):
    groups = []
    for i in range(shift_by):
        groups.append(text[i::shift_by])
    return groups


def potential_indexes_of_mutual_ics(mutual_ics):
    """ Returns potential SET (unique values) of mutual ic values """
    indexes = []
    while mutual_ics:
        mics_list = mutual_ics.pop()
        potential_value = max(mics_list.get("mic"))
        potential_index = mics_list.get("mic").index(potential_value)
        indexes.append({"x": mics_list.get("x"), "y": mics_list.get("y"), "p_index": potential_index, "p_val": potential_value})
    return indexes

def decrypt(text, key, from_index):
    decrypted = ""
    alphabet = letters()
    shifted = [shift(alphabet, -k-from_index) for k in key]
    key_to_use = 0
    for i in range(len(text)):
        orig_index = alphabet.index(text[i])
        decrypted += shifted[key_to_use][orig_index]
        key_to_use = (key_to_use+1)%(len(key))

    return decrypted


def try_decrypt(encrypted, nlet_index=1):
    """ Tries to decrypt the message

    :param text: cyphertext to decrypt
    :param nlet_index: Starting from first nlet as default
    :return: potential decrypted message
    """

    # Find the ngrams
    ngrams = nlets(encrypted, 3)
    # Count the frequency of each ngram
    ngrams_freq = frequency(ngrams)
    #ngram = ngrams[nlet_index]
    # take the ngram starting from most common by index
    ngram, _ = ngrams_freq.most_common(nlet_index)[-1]
    # Find the positions of the chosen ngram
    positions = indexes(encrypted, ngram)
    # find the differences of the positions
    i_diff = index_diff(positions.copy())
    # Find the gcd of those index diffs
    gcd_val = gcd(*i_diff)
    # Group the encrypted text into groups by gcd value (length)
    groups = shift_groups(encrypted, gcd_val)
    # Get the ics of all groups and print to see if they make sense
    ics_vals = ics(groups.copy())
    # Get the mutual ics
    mics = mutual_ic_shift_all(groups.copy())
    # Get the potential indexes of mutual ics
    return potential_indexes_of_mutual_ics(mics.copy())


