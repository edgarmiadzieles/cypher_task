### About the script
The main configuration can be done in main.py by replacing the variable `encrytped` with your own cryptogram.<br>
As an example the `encryped` variable is populated with preexisting cryptogram and correct key is given for that cryptogram.<br>
The answer for that key is listed as 12 in the outputted text after running the script.
##
### Usage:
  * Replace the `encrypted` variable with your own
  * Run script by `python main.py`
  * Output gives the differences between found mutual indexes
  * Solve the equasion by hand and replace they `keys` variable with found keys
  * Run `python main.py` again to see the results
  
### Info
All of the steps for decryption are documented in the try_decrypt function.